//
//  AppDelegate.swift
//  Json2Model
//
//  Created by kolli Praneeth on 6/19/17.
//  Copyright © 2017 kolli Praneeth. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

